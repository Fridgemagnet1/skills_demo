variable "ami" {
  default = "ami-09ee0944866c73f62"
  type = string
  description = "AMI used for the EC2"
}

variable "instance_type" {
  default = "t2.micro"
  type = string
  description = "Instance typeused for the EC2"
}

variable "region" {
  default = "eu-west-2"
  type = string
  description = "AWS region"
}