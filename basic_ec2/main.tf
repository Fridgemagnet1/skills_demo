resource "aws_instance" "app_server" {
  ami             = var.ami
  instance_type   = var.instance_type
  key_name        = "docker_ec2_key"
  security_groups = ["${aws_security_group.allow_ssh.name}"]
  user_data = "${file("file.sh")}"
  tags = {
    Name = "docker-ec2"
  }
}

#resource "aws_security_group" "Docker" {
 # tags = {
  #  type = "terraform-test-security-group"
  #}
#}

resource "aws_security_group" "allow_ssh" {
  name        = "allow_ssh"
  description = "Allow SSH to EC2"

  ingress {
    description      = "SSH to EC2"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    prefix_list_ids = []
  }
} 

#resource "aws_key_pair" "mac-key" {
 #   key_name = "mac"
  #  public_key = file(".ssh/ssh.pub")
#}
resource "aws_key_pair" "docker_ec2_key" {
  key_name = "docker_ec2_key"
  public_key = tls_private_key.rsa.public_key_openssh
}

resource "tls_private_key" "rsa" {
  algorithm = "RSA"
  rsa_bits = 4096
}

resource "local_file" "docker_ec2_key" {
  content = tls_private_key.rsa.private_key_pem
  filename = "docker-ec2-key"
}