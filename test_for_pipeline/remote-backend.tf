terraform {
  backend "s3" {
      bucket = "terraform-state-fridgemagnet-tf-test"
      key = "global/s3/terraform.tfstate"
      region = "eu-west-2"
      dynamodb_table = "terraform-state-locking-tf-test"
      encrypt = true
  }
}

resource "aws_s3_bucket" "terraform_state" {
  bucket = "terraform-state-fridgemagnet-tf-test"

  lifecycle {
    prevent_destroy = true
  }

  versioning {
    enabled = true
  }

  server_side_encryption_configuration {
    rule {
        apply_server_side_encryption_by_default {
            sse_algorithm = "AES256"
        }
    }
  }
}

resource "aws_dynamodb_table" "terraform_locks" {
  name = "terraform-state-locking-tf-test"
  billing_mode = "PAY_PER_REQUEST"
  hash_key = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }
}
